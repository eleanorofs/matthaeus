use crate::matthaeus::dto::watch_list_item::WatchListItem;
use rusqlite::{Connection, Error, Rows};

pub fn get_watch_list(
    connection: Connection
) -> Result<Vec<WatchListItem>, Error> {
    let mut statement = connection.prepare("select * from watch_list;")?;
    let watch_list_rows = statement.query_map([], |row| {
        Ok(WatchListItem { ticker_symbol: row.get(0)? })
    })?;
    let watch_list_results: Vec<Result<WatchListItem, Error>> =
        watch_list_rows.into_iter().collect();
    watch_list_results.into_iter().collect()
}

pub fn initialize_if_necessary(connection: &Connection) -> Result<(), Error> {
    let mut watch_list_exists_statement = connection.prepare(
	"SELECT count(*) FROM sqlite_master
         WHERE type='table' AND name='watch_list';"
    )?;
    
    let mut rows: Rows<'_> = watch_list_exists_statement.query([])?;
    let mut watch_list_exists: i32 = 0;
    match rows.next()? {
        None => {
            watch_list_exists = 0;
        },
        Some(row) => {
            watch_list_exists = row.get(0)?; 
        }
    };
    if watch_list_exists == 0 {
        let create_statement =
	    "create table if not exists watch_list(
                 ticker_symbol text primary key     
             );";
        let _ = connection.execute(create_statement, ())?;
        let insert_statement =
            "insert into watch_list (ticker_symbol) values (:symbol)";
        let _ = connection.execute(insert_statement, &[(":symbol", "QQQ")])?;
        let _ = connection.execute(insert_statement, &[(":symbol", "SPY")])?;
        Ok(())
    }
    else { Ok(()) }
}
