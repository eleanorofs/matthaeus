pub enum LoadableQuote {
    Loading,
    Quote(yahoo_finance_api::Quote),
    Error(yahoo_finance_api::YahooError)
}
