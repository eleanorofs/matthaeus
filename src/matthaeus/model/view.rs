use crate::matthaeus::model::loadable_quote::LoadableQuote;

pub enum View {
    Loading,
    TransactionView,
    WatchListView(Vec<LoadableQuote>)
}
