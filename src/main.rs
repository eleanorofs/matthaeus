mod matthaeus;

use iced::{Application, Command, Element, Length, Renderer, Settings};
use iced::widget::{Column, Row, Text};
use rusqlite::Connection;
pub fn main() -> iced::Result {
    App::run(Settings::with_flags(AppFlags {
	rusqlite: Connection::open("matthaeus.db")
	    .expect("Unable to open local database.")
    }))
}

struct App {
    addressee: String,
    rusqlite: Connection
}

struct AppFlags {
    rusqlite: Connection
}

#[derive(Clone, Debug)]
enum AppMessage {
    TextBoxChange(String)
}

impl Application for App {
    type Executor = iced::executor::Default;
    type Flags = AppFlags;
    type Message = AppMessage;
    type Theme = iced::Theme;

    fn new(flags: AppFlags) -> (App, Command<Self::Message>) {
        let _ = crate::matthaeus::db::repository::
        initialize_if_necessary(&flags.rusqlite);
        (App {
            addressee: String::from("world"),
            rusqlite: flags.rusqlite
        }, Command::none() )
    }

    fn title(&self) -> String {
        String::from("Greet the World")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
	match message {
	    AppMessage::TextBoxChange(string) => {
		self.addressee = string;
		Command::none()
	    }
	}
    }
    
    fn view(&self) -> Element<Self::Message> {
	let text = Text::new(format!("Hello, {0}.", self.addressee))
            .width(Length::Fill)
            .horizontal_alignment(iced::alignment::Horizontal::Center);
	let row1 = Row::new().push(text);
	let text_input: iced::widget::TextInput<'_, AppMessage, Renderer> =
	    iced::widget::text_input("world", self.addressee.as_str())
	    .on_input(AppMessage::TextBoxChange);
	let row2 = Row::new().push(text_input);
	Column::new().push(row1).push(row2).into()
    }
}
