# Matthaeus: Finance software for GNOME

### Development

Requires libsqlite3. 

```bash
sudo apt install libsqlite3-dev
```

#### Build

```bash
cargo build
```

#### Run

```bash
cargo run
```
